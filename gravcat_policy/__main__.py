# -*- coding: utf-8 -*-
# Copyright (C) 2021  James Alexander Clark <james.clark@ligo.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Rucio plugin algorithm for LFN to PFN conversion for rucio in the
International Gravitational Wave Network.

This program provides a commandline client to determine and check the LFN2PFN
algorithm.
"""

import argparse
import sys
from inspect import getmembers, isfunction

import argcomplete

from . import (__version__, lfn2pfn)

__author__ = 'James Alexander Clark <james.clark@ligo.org>'


def parse_inputs():
    """Command line parser
    """

    args = argparse.ArgumentParser(description=__doc__)

    args.add_argument(dest='lfn',
                      metavar='LFN',
                      help="""Logical filename."""
                      )

    args.add_argument('-v', '--version', action='version',
                      version=__version__,
                      help="""show version number and exit""")

    args.add_argument('-s', '--scope',
                      metavar='SCOPE',
                      required=True,
                      help="""show version number and exit""")

    args.add_argument('-a', '--algorithm',
                      metavar='ALGORITHM',
                      default='igwn_frames',
                      help="""Name of igwn_lfn2pfn algorithm to use."""
                      )

    argcomplete.autocomplete(args)

    args = args.parse_args(sys.argv[1:])

    return args


def main():
    """Run the thing
    """

    # Parse input
    args = parse_inputs()

    # All available lfn2pfn functions
    all_funcs = dict((name, func) for name, func in getmembers(lfn2pfn, isfunction))

    # TODO: support options for native rucio lfn2pfn algorithm(s)

    # Generate and print PFN
    print(all_funcs[args.algorithm](args.scope, args.lfn), file=sys.stdout)


if __name__ == "__main__":
    sys.exit(main())

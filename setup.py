#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2021 James Alexander Clark <james.clark@ligo.org>
#
# This file is part of gravcat_policy
#
# GWDataFind is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GWDataFind is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GWDataFind.  If not, see <http://www.gnu.org/licenses/>.

"""Setup gravcat_policy
"""

from setuptools import setup
setup()
